package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.model.User;

import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.NotNull;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    boolean existsById(@NotNull String id);

    User removeUser(User user);

    User removeUserById(@Nullable String id);

    User removeUserByLogin(@Nullable String login);

    boolean existsByLogin(@Nullable String login);

    boolean existsByEmail(@Nullable String email);

    void setPasswordById(@NotNull String id, @NotNull String password);

}
