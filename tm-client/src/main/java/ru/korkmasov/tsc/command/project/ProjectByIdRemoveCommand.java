package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.model.Project;
import ru.korkmasov.tsc.model.User;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class ProjectByIdRemoveCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        @NotNull final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectService().findOneById(user.getId(), id);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectTaskService().removeProjectById(user.getId(), id);
    }

}
