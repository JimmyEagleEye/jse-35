package ru.korkmasov.tsc.exception.system;

import ru.korkmasov.tsc.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not found...");
    }

}
