package ru.korkmasov.tsc;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.korkmasov.tsc.endpoint.SessionEndpoint;
import ru.konovalov.tm.marker.SoapCategory;
import ru.korkmasov.tsc.model.Session;
import ru.korkmasov.tsc.model.User;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpointService = new SessionEndpoint();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void testCloseSession() {
        @NotNull final Session session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
        @NotNull final User user = sessionEndpoint.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
        sessionEndpoint.closeSession(session);
        Assert.assertNull(sessionEndpoint.getUser(session));
    }

    @Test
    @Category(SoapCategory.class)
    public void testIncorrect() {
        @NotNull final Session session = sessionEndpoint.openSession("qweqwe", "123123");
        Assert.assertNull(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void testOpenSession() {
        @NotNull final Session session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
        @NotNull final User user = sessionEndpoint.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

}
